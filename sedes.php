<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php include "components/heads.php"; ?>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
  <link rel="stylesheet" href="./assets/css/swiper.css">
</head>

<body>
  <?php include "./components/navbar.php" ?>
  <div class="mx-auto max-w-7xl px-2 pt-4">

    <?php
    $stmt = $DB_con->prepare("SELECT * FROM sedes");
    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
    ?>
        <div class="mb-8 mt-4 rounded-xl p-2 shadow-md shadow-blue-200">
          <div class="grid grid-cols-2 gap-8">
            <div class="w-11/12">
              <div style="border-radius: 20px;" class="swiper mySwiper">
                <div class="swiper-wrapper">
                  <?php
                  if (!($img2 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img'] . '') ?>">
                    </div>
                  <?php } else { ?>
                    <div style="width: 100%; display:flex !important; justify-content: center !important;">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img'] . '') ?>">
                    </div>
                  <?php } ?>


                  <?php
                  if (!($img2 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img2'] . '') ?>">
                    </div>
                  <?php } ?>


                  <?php
                  if (!($img3 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img3'] . '') ?>">
                    </div>
                  <?php } ?>

                  <?php
                  if (!($img4 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img4'] . '') ?>">
                    </div>
                  <?php } ?>

                  <?php
                  if (!($img5 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img5'] . '') ?>">
                    </div>
                  <?php } ?>
                  <style>
                    .img-fluid {
                      height: 300px !important;
                    }
                  </style>

                </div>
                <div class="swiper-pagination"></div>
              </div>

              <!-- Swiper JS -->
              <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>


            </div>
            <div>
              <h1 class="text-center text-3xl font-extrabold leading-9 tracking-tight text-blueapcef sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
                <?php echo "Subsede " . $nome;
                ?>
              </h1>
              <h2 style="font-size: 20px;" class="title-font mb-1 pt-4 text-lg font-semibold blueApcef">
                Localização
              </h2>
              <h2 style="font-size: 18px;" class="title-font text-md mb-1"><?php echo $localização ?></h2>
              <h2 style="font-size: 20px;" class="title-font mb-1 pt-4 text-lg font-semibold blueApcef">
                Infraestrutura
              </h2>
              <h2 class="title-font text-md mb-1">
                <h2 style="font-size: 18px;" class="title-font text-md mb-1"><?php echo $infraestrutura ?></h2>
              </h2>
              <a href="<?php echo $URI->base('/sede/' . slugify($nome)); ?>">
                <button class="shadow-cla-blue mt-4 mb-4 rounded-lg bg-blueapcef px-4 py-1 text-white drop-shadow-md hover:scale-105">
                  Saiba Mais
                </button>
              </a>
            </div>
          </div>
        </div><?php }
          } ?>
  </div>
  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper(".mySwiper", {
      pagination: {
        el: ".swiper-pagination",
        dynamicBullets: true,
      },
    });
  </script>
  <?php include "./components/footer.php" ?>
</body>

</html>