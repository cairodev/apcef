<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
$URI = new URI();


function remove_simbolos_acentos($string)
{
  $string = trim(strtolower($string));
  $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýýþÿŔŕ?';
  $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuuyybyRr-';
  $string = strtr($string, utf8_decode($a), $b);
  $string = str_replace(".", "-", $string);
  $string = preg_replace("/[^0-9a-zA-Z\.]+/", '-', $string);
  return utf8_decode(rtrim($string, "-"));
}


$url = explode("/", $_SERVER['REQUEST_URI']);
$idPost = $url[4];

$idPost2 = "";



$stmt = $DB_con->prepare("SELECT * FROM esportes");
$stmt->execute();
if ($stmt->rowCount() > 0) {
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($row);
    $string1 = remove_simbolos_acentos(utf8_decode($idPost));
    $string2 = remove_simbolos_acentos(utf8_decode($esporte));
    if ($string1 == $string2) {
      $idPost2 = $esporte;
    }
  }
}


$stmt = $DB_con->prepare("SELECT esporte FROM esportes where esporte='$idPost2' ORDER BY id DESC");
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  extract($row);
  $post = $esporte;
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php include "components/heads.php"; ?>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
  <link rel="stylesheet" href="./assets/css/swiper.css">
</head>

<style>
  .swiper {
    width: 100%;
    height: 100%;
  }

  .swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;

    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  }

  .swiper-slide img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }



  .swiper {
    width: 100%;
    height: 300px;
    margin-left: auto;
    margin-right: auto;
  }

  .swiper-slide {
    background-size: cover;
    background-position: center;
  }

  .mySwiper2 {
    height: 100%;
    width: 100%;

  }

  .mySwiper2 .swiper-wrapper .swiper-slide img {
    max-width: 700px;
    max-height: 700px;
    width: auto;
    height: auto;
  }

  .mySwiper {
    height: 20%;
    box-sizing: border-box;
    padding: 10px 0;
  }

  .mySwiper .swiper-slide {
    width: 25%;
    height: 100%;
    opacity: 0.4;
  }

  .mySwiper .swiper-slide-thumb-active {
    opacity: 1;
  }

  .swiper-slide img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
</style>

<body>
  <?php include "./components/navbar.php" ?>
  <main>
    <?php
    $stmt = $DB_con->prepare("SELECT * FROM esportes where esporte='$post'");
    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
    ?>
        <div class="mx-auto max-w-7xl px-2 pt-4">
          <h1 style="color: #013589;" class="text-center text-3xl font-extrabold leading-9 tracking-tight dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
            <?php echo $esporte; ?>
          </h1>

          <div class="mb-8 mt-4 rounded-xl p-2 shadow-md shadow-blue-200">


            <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff;" class="swiper mySwiper2">
              <div class="swiper-wrapper">

                <?php
                if (!($img2 == "")) { ?>
                  <div class="swiper-slide">
                    <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/esportes/' . $row['img'] . '') ?>">
                  </div>
                <?php } else { ?>
                  <div style="width: 100%; display:flex !important; justify-content: center !important;">
                    <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/esportes/' . $row['img'] . '') ?>">
                  </div>
                <?php } ?>


                <?php
                if (!($img2 == "")) { ?>
                  <div class="swiper-slide">
                    <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/esportes/' . $row['img2'] . '') ?>">
                  </div>
                <?php } ?>


                <?php
                if (!($img3 == "")) { ?>
                  <div class="swiper-slide">
                    <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/esportes/' . $row['img3'] . '') ?>">
                  </div>
                <?php } ?>


              </div>
              <?php
              if (!($img2 == "" && $img3 == "")) { ?>
                <div style="filter: invert(40%);" class="swiper-button-next"></div>
                <div style="filter: invert(40%);" class="swiper-button-prev"></div>


              <?php } ?>
              <style>
                .img-fluid {
                  height: 400px !important;
                }
              </style>
            </div>
            <div thumbsSlider="" class="swiper mySwiper">

              <script>
                var swiper = new Swiper(".mySwiper", {
                  loop: true,
                  spaceBetween: 10,
                  slidesPerView: 4,
                  freeMode: true,
                  watchSlidesProgress: true,
                });
                var swiper2 = new Swiper(".mySwiper2", {
                  loop: true,
                  spaceBetween: 10,
                  navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                  },
                  thumbs: {
                    swiper: swiper,
                  },
                });
              </script>
            </div>
            <?php if (!($professores == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  Professores: <?php echo $professores; ?>
                </h2>
              </div>
            <?php  } ?>
            <?php if (!($turma1 == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  <strong>Turmas:</strong>
                </h2>
                <h2 class="title-font text-md mb-1">
                  <?php echo $turma1; ?>
                </h2>
              </div>
            <?php  } ?>

            <?php if (!($turma2 == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  <?php echo $turma2; ?>
                </h2>
              </div>
            <?php  } ?>

            <?php if (!($turma3 == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  <?php echo $turma3; ?>
                </h2>
              </div>
            <?php  } ?>

            <?php if (!($turma4 == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  <?php echo $turma4; ?>
                </h2>
              </div>
            <?php  } ?>
            <?php if (!($turma5 == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  <?php echo $turma5; ?>
                </h2>
              </div>
            <?php  } ?>

            <?php if (!($turma6 == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  <?php echo $turma6; ?>
                </h2>
              </div>
            <?php  } ?>

            <?php if (!($turma7 == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  <?php echo $turma7; ?>
                </h2>
              </div>
            <?php  } ?>

            <?php if (!($turma8 == "")) { ?>
              <div>
                <h2 class="title-font text-md mb-1">
                  <?php echo $turma8; ?>
                </h2>
              </div>
            <?php  } ?>

          </div>
        </div>
    <?php
      }
    }
    ?>
  </main>
  <?php include "./components/footer.php" ?>

</body>

</html>