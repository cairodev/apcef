<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<?php include "components/heads.php"; ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
	<link rel="stylesheet" href="./assets/css/swiper.css">
</head>

<body>
	<?php include "components/navbar.php"; ?>

	<div class="mx-auto max-w-7xl px-2 pt-8">
		<?php
		$stmt = $DB_con->prepare("SELECT * FROM about");
		$stmt->execute();
		if ($stmt->rowCount() > 0) {
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
		?>
				<div class="md:grid gap-8 p-2">
					<h1 class="text-center text-3xl font-extrabold leading-9 tracking-tight text-blueapcef sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
						<?php echo $nome; ?>
					</h1>
					<p class="text-lg leading-7 text-justify">
						<?php echo $texto; ?>
					</p>
			<?php }
		} ?>
				</div>
	</div>

	<?php include "./components/footer.php" ?>
</body>

</html>