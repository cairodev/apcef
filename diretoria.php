<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<?php include "components/heads.php"; ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
	<link rel="stylesheet" href="./assets/css/swiper.css">
</head>

<body>
	<?php include "./components/navbar.php" ?>
	<div class="mx-auto max-w-7xl px-2 pt-4">
		<h1 class="text-center text-3xl font-extrabold leading-9 tracking-tight text-blueapcef sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
			DIRETORIA
		</h1>
		<div class="grid grid-cols-4 gap-4">
			<?php
			$stmt = $DB_con->prepare("SELECT * FROM funcionarios WHERE setor='diretoria'");
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
			?>
					<div class="mb-8 mt-4 flex h-28 items-center rounded-xl p-2 shadow-md shadow-blue-200">
						<div class="mr-3">
							<img src="<?php echo $URI->base('./assets/img/logo.png') ?>" alt="avatar" style="width: 50px !important; height:50px !important;" class="h-48 w-48 rounded-full" />
						</div>
						<div>
							<h1 class="title-font mb-1 text-lg font-medium"><?php echo $nome; ?></h1>
							<h2 class="title-font text-md mb-3 font-medium text-gray-400"><?php echo $cargo; ?></h2>
						</div>
					</div>
			<?php }
			} ?>
		</div>
		<h1 class="text-center text-3xl font-extrabold leading-9 tracking-tight text-blueapcef sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
			CONSELHO DELIBERATIVO
		</h1>
		<div class="grid grid-cols-4 gap-4">
			<?php
			$stmt = $DB_con->prepare("SELECT * FROM funcionarios WHERE setor='conselho deliberativo'");
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
			?>
					<div class="mb-8 mt-4 flex h-28 items-center rounded-xl p-2 shadow-md shadow-blue-200">
						<div class="mr-3">
							<img src="<?php echo $URI->base('./assets/img/logo.png') ?>" alt="avatar" style="width: 50px !important; height:50px !important;" class="h-48 w-48 rounded-full" />
						</div>
						<div>
							<h1 class="title-font mb-1 text-lg font-medium"><?php echo $nome; ?></h1>
							<h2 class="title-font text-md mb-3 font-medium text-gray-400"><?php echo $cargo; ?></h2>
						</div>
					</div>
			<?php }
			} ?>
		</div>

		<h1 class="text-center text-3xl font-extrabold leading-9 tracking-tight text-blueapcef sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
			CONSELHO FISCAL
		</h1>
		<div class="grid grid-cols-4 gap-4">
			<?php
			$stmt = $DB_con->prepare("SELECT * FROM funcionarios WHERE setor='conselho fiscal'");
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
			?>
					<div class="mb-8 mt-4 flex h-28 items-center rounded-xl p-2 shadow-md shadow-blue-200">
						<div class="mr-3">
							<img src="<?php echo $URI->base('./assets/img/logo.png') ?>" alt="avatar" style="width: 50px !important; height:50px !important;" class="h-48 w-48 rounded-full" />
						</div>
						<div>
							<h1 class="title-font mb-1 text-lg font-medium"><?php echo $nome; ?></h1>
							<h2 class="title-font text-md mb-3 font-medium text-gray-400"><?php echo $cargo; ?></h2>
						</div>
					</div>
			<?php }
			} ?>
		</div>

	</div>
	<?php include "./components/footer.php" ?>
</body>

</html>