<div class="lg:grid lg:grid-cols-3 max-w-screen-xl mx-auto px-2 space-x-5">
	<div class="pt-10 lg:col-span-2">
		<div class="space-y-2 p-2 pb-2 md:space-y-2 lg:flex lg:space-x-4">
			<h1 class="text-center md:text-left text-3xl font-bold">
				Notícias APCEF/PI
			</h1>
			<p class="text-lg leading-7 text-gray-600 sm:text-center md:text-left">
				Acompanhe as notícias mais atualizadas de nossa APCEF
			</p>
		</div>
		<section class="swiper swiper_posts">
			<div class="swiper-wrapper">
				<?php $stmt = $DB_con->prepare("SELECT * FROM posts order by id desc");
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
				?>
					<div class="swiper-slide my-4 mx-1">
						<div class="max-w-lg p-3 mx-auto rounded-md shadow-csc">
							<div class="rounded-2xl">
								<h3 class="post-title mb-4 text-lg font-black py-2"><?php echo $title; ?></h3>
							</div>
							<div>
								<img class="rounded-md md:h-52 h-32" src="./admin/uploads/posts/<?php echo $img; ?>">
							</div>
							<div class="post-info">
								<?php echo $info ?>
							</div>
							<div class="flex justify-center">
								<a href="<?php echo $URI->base('/noticia/' . slugify($title)); ?>" class="text-white bg-color2 focus:ring-4 rounded-md font-md text-md px-5 py-2 text-center">Saiba mais</a>
							</div>
						</div>
					</div>
				<?php
				}
				?>
			</div>
		</section>
	</div>
	<div class="lg:pt-20">
		<div class="overflow-hidden rounded-xl shadow-md shadow-gray-300">
				<img src="./assets/img/saude.png" alt="APCEF Saúde" />
		</div>
		<div class="flex justify-center pt-4">
			<button class="text-white bg-color3 focus:ring-4 rounded-md font-md text-md px-5 py-2 text-center">
				Entre em contato agora
			</button>
		</div>
	</div>
</div>
<div class="lg:grid lg:grid-cols-3 max-w-screen-xl mx-auto px-2 space-x-5">
	<div class="pt-10 lg:col-span-2">
		<div class="space-y-2 p-2 pb-2 md:space-y-2 lg:flex lg:space-x-4">
			<h1 class="text-center md:text-left text-3xl font-bold">
				Notícias FENAE
			</h1>
			<p class="text-lg leading-7 text-gray-600 sm:text-center md:text-left">
				Acompanhe as notícias mais atualizadas de nossa Federação
			</p>
		</div>
		<section class="swiper swiper_posts">
			<div class="swiper-wrapper">
				<?php $stmt = $DB_con->prepare("SELECT * FROM posts order by id desc");
				$stmt->execute();
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
				?>
					<div class="swiper-slide my-4 mx-1">
						<div class="max-w-lg p-3 mx-auto rounded-md shadow-csc">
							<div class="rounded-2xl">
								<h3 class="post-title mb-4 text-lg font-black py-2"><?php echo $title; ?></h3>
							</div>
							<div>
								<img class="rounded-md md:h-52 h-32" src="./admin/uploads/posts/<?php echo $img; ?>">
							</div>
							<div class="post-info">
								<?php echo $info ?>
							</div>
							<div class="flex justify-center">
								<a href="#" class="text-white bg-color2 focus:ring-4 rounded-md font-md text-md px-5 py-2 text-center">Saiba mais</a>
							</div>
						</div>
					</div>
				<?php
				}
				?>
			</div>
		</section>
	</div>
	<div class="lg:pt-20">
		<div class="overflow-hidden rounded-xl shadow-md shadow-gray-300">
				<img src="./assets/img/associase.png" alt="Associe-se" />
		</div>
		<div class="flex justify-center pt-4">
			<button class="text-white bg-color1 focus:ring-4 rounded-md font-md text-md px-5 py-2 text-center">
				Entre em contato agora
			</button>
		</div>
	</div>
</div>