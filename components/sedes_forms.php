<div class="lg:grid lg:grid-cols-3 max-w-screen-xl mx-auto px-2 space-x-5">
  <div class="pt-10 lg:col-span-2">
    <div class="space-y-2 p-2 pb-2 md:space-y-2 lg:flex lg:space-x-4">
      <h1 class="text-center md:text-left text-3xl font-bold">
        Conheça Nossa Sede
      </h1>
    </div>
    <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper mySwiper4">
      <div class="swiper-wrapper">
        <?php
        $stmt = $DB_con->prepare("SELECT * FROM sedes WHERE nome = 'teresina' ");
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
          while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            extract($row);
        ?>
            <?php if (!($img == "")) { ?>
              <div class="swiper-slide">
                <img src="./admin/uploads/sedes/<?php echo $row['img']; ?>" alt="<?php echo $nome ?>" />
              </div>
            <?php } ?>

            <?php if (!($img2 == "")) { ?>
              <div class="swiper-slide">
                <img src="./admin/uploads/sedes/<?php echo $row['img2']; ?>" alt="<?php echo $nome ?>" />
              </div>
            <?php } ?>

            <?php if (!($img3 == "")) { ?>
              <div class="swiper-slide">
                <img src="./admin/uploads/sedes/<?php echo $row['img3']; ?>" alt="<?php echo $nome ?>" />
              </div>
            <?php } ?>

            <?php if (!($img4 == "")) { ?>
              <div class="swiper-slide">
                <img src="./admin/uploads/sedes/<?php echo $row['img4']; ?>" alt="<?php echo $nome ?>" />
              </div>
            <?php } ?>

            <?php if (!($img5 == "")) { ?>
              <div class="swiper-slide">
                <img src="./admin/uploads/sedes/<?php echo $row['img5']; ?>" alt="<?php echo $nome ?>" />
              </div>
            <?php } ?>
      </div>
      <div style="filter: invert(40%);" class="swiper-button-next"></div>
      <div style="filter: invert(40%);" class="swiper-button-prev"></div>
    </div>
    <div thumbsSlider="" class="swiper mySwiper3">
      <div class="swiper-wrapper">

        <?php if (!($img == "")) { ?>
          <div class="swiper-slide">
            <img src="./admin/uploads/sedes/<?php echo $row['img']; ?>" alt="<?php echo $nome ?>" />
          </div>
        <?php } ?>
        <?php if (!($img2 == "")) { ?>
          <div class="swiper-slide">
            <img src="./admin/uploads/sedes/<?php echo $row['img2']; ?>" alt="<?php echo $nome ?>" />
          </div>
        <?php } ?>
        <?php if (!($img3 == "")) { ?>
          <div class="swiper-slide">
            <img src="./admin/uploads/sedes/<?php echo $row['img3']; ?>" alt="<?php echo $nome ?>" />
          </div>
        <?php } ?>
        <?php if (!($img4 == "")) { ?>
          <div class="swiper-slide">
            <img src="./admin/uploads/sedes/<?php echo $row['img4']; ?>" alt="<?php echo $nome ?>" />
          </div>
        <?php } ?>

        <?php if (!($img5 == "")) { ?>
          <div class="swiper-slide">
            <img src="./admin/uploads/sedes/<?php echo $row['img5']; ?>" alt="<?php echo $nome ?>" />
          </div>
        <?php } ?>

      </div>
    </div>
<?php
          }
        }
?>
  </div>
  <div class="lg:pt-14">
    <div class="mx-auto max-w-md space-y-6">
      <form action="">
        <h2 class="text-center md:text-left text-3xl font-bold">Informações APCEF/PI</h2>
        <p class="my-4 opacity-70">
          Receba informações da APCEF/PI pelo WhatsApp. É só cadastrar seu
          número com DDD.
        </p>
        <label class="text-sm font-bold uppercase opacity-70">
          Nome Completo
        </label>
        <input type="text" class="mt-2 mb-4 w-full rounded p-3 border border-gray-300 text-gray-900" />
        <label class="text-sm font-bold uppercase opacity-70">
          WhatsApp
        </label>
        <input type="text" class="mt-2 mb-4 w-full rounded p-3 border border-gray-300 text-gray-900" />
        <label class="text-sm font-bold uppercase opacity-70">
          Email
        </label>
        <input type="text" class="mt-2 mb-4 w-full rounded p-3 border border-gray-300 text-gray-900" />
        <label class="text-sm font-bold uppercase opacity-70">
          Cidade
        </label>
        <input type="text" class="mt-2 mb-4 w-full rounded p-3 border border-gray-300 text-gray-900" />
        <input type="submit" class="my-2 cursor-pointer rounded bg-color2 py-3 px-6 font-medium  text-white duration-300 ease-in-out hover:bg-blue-apcef" value="Enviar">
      </form>
    </div>
  </div>
</div>