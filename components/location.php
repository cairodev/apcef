<div class="mx-auto max-w-4xl px-10 loc">
	<img src="<?php echo $URI->base("/assets/img/barra_lateral.png"); ?>" class="barra_lateral" alt="" />
	<div class="loc_title">
		<h1 class="lg:text-5xl text-4xl">Nossa</h1>
		<span class="lg:text-7xl text-5xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2"> Localização </span>
	</div>
</div>
<div class="max-w-screen-xl mx-auto lg:pt-10">
	<div class="items-center lg:grid lg:grid-cols-2">
		<div class="border-4 border-color1 rounded-3xl m-10">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3974.1397110376697!2d-42.81690228619596!3d-5.081093196312187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x78e39e1aa0e31e7%3A0x696c0d926fa46758!2sITTNET!5e0!3m2!1spt-BR!2sbr!4v1567994506060!5m2!1spt-BR!2sbr" width="100%" height="250" frameborder="0" style="border-radius:20px;" allowfullscreen=""></iframe>
		</div>
		<div class="lg:pt-4 px-10">
			<h2 class="text-2xl">Av. <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">Miguel Rosa, 1650</span> - Centro (Norte)
				Teresina - PI, 64000-480</h2>
			<h2 class="text-2xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">vendas@ittnet.com.br</h2>
			<h2 class="text-2xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">3131-8000</h2>
		</div>
	</div>

	<div class="items-center lg:grid lg:grid-cols-2">
		<div class="border-4 border-color1 rounded-3xl m-10">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15938.734695624491!2d-41.7724834!3d-2.9071198!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x34d7972c3da59c78!2sITTNET%20PRIME!5e0!3m2!1spt-BR!2sbr!4v1672187608769!5m2!1spt-BR!2sbr" width="100%" height="250" style="border-radius:20px;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
		</div>
		<div class="lg:pt-4 px-10">
			<h2 class="text-2xl">R. <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">Francisco Aires, 18 - FÁTIMA</span> - Parnaíba - PI, 64202-120</h2>
			<h2 class="text-2xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">vendas@ittnet.com.br</h2>
			<h2 class="text-2xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">3131-8000</h2>
		</div>
	</div>
</div>