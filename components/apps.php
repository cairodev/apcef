<div class="mx-auto max-w-4xl pt-10" id="aplicativos">
  <h1 class="lg:text-5xl text-2xl text-center">Temos <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2"> planos novos todos os meses, </span>
    Fale com um de <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">nossos atendentes!</span></h1>
  <div class="items-center lg:grid lg:grid-cols-3">
    <div class="lg:col-span-2 pt-10">
      <img class="w-full" src="<?php echo $URI->base("/assets/img/celulares_com_apps.png"); ?>" alt="Mascote ITTNET" />
    </div>
    <div class="lg:pt-4 lg:p-0 p-10">
      <h2 class="lg:text-4xl text-3xl">Baixe já os nossos Apps! ITTPlay <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">TV</span> e <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2">ITTNET!</span></h2>
      <a href="https://onelink.to/4wukcx">
        <img src="<?php echo $URI->base("/assets/img/google.png"); ?>" ?>
        <img src="<?php echo $URI->base("/assets/img/applestore.png"); ?>" ?>
      </a>
    </div>
  </div>
</div>