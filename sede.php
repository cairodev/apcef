<?php
require_once "dbconfig.php";
require "classes/Helper.php";
require "classes/Url.class.php";
$URI = new URI();
//error_reporting(~E_ALL);

function remove_simbolos_acentos($string){
  $string = trim(strtolower($string));
  $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýýþÿŔŕ?';
  $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuuyybyRr-';
  $string = strtr($string, utf8_decode($a), $b);
  $string = str_replace(".","-",$string);
  $string = preg_replace( "/[^0-9a-zA-Z\.]+/",'-',$string);
  return utf8_decode(rtrim($string, "-"));
}


$url = explode("/", $_SERVER['REQUEST_URI']);
$idPost = $url[3];

$idPost2 = "";



$stmt = $DB_con->prepare("SELECT * FROM sedes");
$stmt->execute();
if ($stmt->rowCount() > 0) {
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($row);
    $string1 = remove_simbolos_acentos(utf8_decode($idPost));
    $string2 = remove_simbolos_acentos(utf8_decode($nome));
    if($string1 == $string2){
      $idPost2 = $nome;
    }
  }}


$stmt = $DB_con->prepare("SELECT nome FROM sedes where nome='$idPost2' ORDER BY id DESC");
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  extract($row);
  $post = $nome;
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
  <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
  <script>
    tailwind.config = {
      theme: {
        extend: {
          colors: {
            clifford: '#da373d',
            orangeapcef: '#E96708',
            blueapcef: '#013589',
            redapcef: '#ed1b24'
          }
        }
      }
    }
  </script>
  <link rel="stylesheet" href="<?php echo $URI->base('/assets/css/style.css') ?>">
  <title><?php echo $idPost2 ?></title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
  
</head>

<body>
  <?php include "./components/nav.php" ?>
  <main>
    <?php
    $stmt = $DB_con->prepare("SELECT * FROM sedes where nome='$post'");
    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
    ?>
  <div class="mx-auto max-w-7xl px-2 pt-4">
        <h1 class="text-center text-3xl font-extrabold leading-9 tracking-tight text-blueapcef sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
          <?php echo $nome; ?>
        </h1>
        <h2 class="title-font text-md mb-1">
<?php echo $descrição; ?>
        </h2>
        <div class="mb-8 mt-4 flex items-center rounded-xl p-2 shadow-md shadow-blue-200">
          <div>
            
          <?php if(!($localização == "")){?>  
          <h2 style="margin-top:-50px" class="title-font mb-1 pt-4 text-lg font-semibold text-blueapcef">
              Localização
            </h2>
            <h2 class="title-font text-md mb-1">
              <?php echo $localização; ?>
            </h2>
            <?php } ?>
            <?php if(!($infraestrutura == "")){?>  
                <h2 class="title-font mb-1 pt-4 text-lg font-semibold text-blueapcef">
              Infraestrutura
            </h2>
            <h2 class="title-font text-md mb-1">
              <?php echo $infraestrutura; ?>
            </h2>
            <?php } ?>
            <?php if(!($diretoria == "")){?>  
                <h2 class="title-font mb-1 pt-4 text-lg font-semibold text-blueapcef">
              Diretoria
            </h2>
            <h2 class="title-font text-md mb-1">
             
            <?php echo $diretoria; ?>
             
            <?php } ?>
            
          </div>


          <div style="border-radius: 20px;" class="swiper mySwiper">
                <div class="swiper-wrapper">
                  <?php
                  if (!($img2 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img'] . '') ?>">
                    </div>
                  <?php } else { ?>
                    <div style="width: 100%; display:flex !important; justify-content: center !important;">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img'] . '') ?>">
                    </div>
                  <?php } ?>


                  <?php
                  if (!($img2 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img2'] . '') ?>">
                    </div>
                  <?php } ?>


                  <?php
                  if (!($img3 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img3'] . '') ?>">
                    </div>
                  <?php } ?>

                  <?php
                  if (!($img4 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img4'] . '') ?>">
                    </div>
                  <?php } ?>

                  <?php
                  if (!($img5 == "")) { ?>
                    <div class="swiper-slide">
                      <img class="img-fluid" src="<?php echo $URI->base('/admin/uploads/sedes/' . $row['img5'] . '') ?>">
                    </div>
                  <?php } ?>
                    <style>
                      .img-fluid{
                        height: 300px !important;
                      }
                    </style>

                </div>
                <div class="swiper-pagination"></div>
              </div>


        </div>
        
                

      </div>
      <!-- Initialize Swiper -->
  <script>
                var swiper = new Swiper(".mySwiper", {
                  pagination: {
                    el: ".swiper-pagination",
                    dynamicBullets: true,
                  },
                });
              </script>
  <?php }} include "./components/footer.php" ?>

</body>

</html>

