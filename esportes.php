<?php
require_once "dbconfig.php";
require "classes/Helper.php";
require "classes/Url.class.php";
$URI = new URI();

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
    <script>
        tailwind.config = {
            theme: {
                extend: {
                    colors: {
                        clifford: '#da373d',
                        orangeapcef: '#E96708',
                        blueapcef: '#013589',
                        redapcef: '#ed1b24'
                    }
                }
            }
        }
    </script>
    <link rel="stylesheet" href="./assets/css/style.css">
    <title>Document</title>
</head>

<body>
    <?php include "./components/nav.php" ?>
    <div class="mx-auto max-w-7xl px-2 pt-4">
        <h1 class="text-blueapcef text-center text-3xl font-extrabold leading-9 tracking-tight sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
            ESPORTES APCEF/PI
        </h1>
        <div class="grid grid-cols-4 gap-8">

            <?php
            $stmt = $DB_con->prepare("SELECT * FROM esportes");
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);

            ?>
                    <div class="mb-8 mt-4 items-center rounded-xl p-2 shadow-md shadow-blue-200">
                        <div class="mr-3">
                            <img class="rounded-md" src="<?php echo $URI->base('/admin/uploads/esportes/' . $row['img'] . '') ?>" alt="avatar" style="width:500px; height:200px;" />
                        </div>
                        <div>
                            <h1 style="margin-top:15px; font-size: 20px;" class="title-font mb-1 text-center text-lg font-bold uppercase blueApcef">
                                <?php echo $esporte ?>
                            </h1>
                            <div class="flex justify-center">
                                <a href="<?php echo $URI->base('/esporte/' . slugify($esporte)); ?>">
                                    <button class="shadow-cla-blue mt-4 mb-4 rounded-lg bg-blueapcef px-4 py-1 text-white drop-shadow-md hover:scale-105">
                                        Saiba Mais
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
            <?php }
            } ?>

        </div>
    </div>
    <?php include "./components/footer.php" ?>
</body>

</html>