<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
$URI = new URI();
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php include "components/heads.php"; ?>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
  <link rel="stylesheet" href="./assets/css/swiper.css">
</head>

<body>
  <?php include "components/navbar.php"; ?>
  <div class="mx-auto max-w-7xl px-2 pt-4">
    <h1 class="text-blueapcef text-center text-3xl font-extrabold leading-9 tracking-tight sm:text-4xl sm:leading-10 md:text-left md:text-3xl md:leading-14">
      Notícias APCEF/PI
    </h1>
    <?php
    $stmt = $DB_con->prepare("SELECT title as title_post, info, img as img_post  FROM posts");
    $stmt->execute();
    if ($stmt->rowCount() > 0) {
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
    ?>
        <div class="grid grid-cols-3 pt-4">
          <div class="mb-4 overflow-hidden rounded-xl">
            <div class="h-60 w-full scale-100">
              <img style="display: block;
            width: 100%;
            height: 330px;
            object-fit: cover;" src="<?php echo $URI->base('/admin/uploads/posts/' . $img_post . '') ?>">
            </div>
          </div>
          <div class="col-span-2 p-6">
            <h1 style="font-size: 23px;" class="text-blueapcef title-font mb-3 text-lg font-medium"> <?php echo $title_post; ?></h1>
            <div class="flex flex-wrap items-center ">
              <a href="<?php echo $URI->base('/noticia/' . slugify($title_post)); ?>">
                <button class="shadow-cla-blue rounded-lg bg-blueapcef px-4 py-1 text-white drop-shadow-md hover:scale-105">
                  Leia mais
                </button>
              </a>
            </div>
          </div>
        </div>
    <?php }
    } ?>
  </div>


  <?php include "./components/footer.php" ?>
</body>

</html>